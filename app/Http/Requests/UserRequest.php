<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => ['required', 'string', 'between:5,255'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($this->input('id')),],
            'password' => ['required_without:id', 'confirmed', 'string', 'min:8', 'nullable'],
            'permissao' => ['in:admin,user',],
        ];

        if (Auth::user()->permissao == 'admin') {
            $rules['permissao'][] = 'required';
        }

        return $rules;
    }
}
