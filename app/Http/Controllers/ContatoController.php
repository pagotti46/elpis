<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContatoRequest;
use App\Models\Contato;
use App\Repositories\ContatoRepository;
use Illuminate\Http\Request;

class ContatoController extends Controller
{
    protected $contatoRepository;

    function __construct(ContatoRepository $contatoRepository)
    {
        $this->contatoRepository = $contatoRepository;
    }

    public function index()
    {
        $this->authorize('viewAny', Contato::class);
        $contatos = $this->contatoRepository->all();

        return view('contatos.index', compact('contatos'));
    }

    public function create()
    {
        $this->authorize('create', Contato::class);

        return view('contatos.form');
    }

    public function store(ContatoRequest $request)
    {
        $this->authorize('create', Contato::class);

        $contato = $this->contatoRepository->store($request);

        return redirect()->route('contatos.show', $contato->id);
    }

    public function show($id)
    {
        $contato = $this->contatoRepository->find($id);
        $this->authorize('view', $contato);

        return view('contatos.show', compact('contato'));
    }

    public function edit($id)
    {
        $contato = $this->contatoRepository->find($id);
        $this->authorize('update', $contato);

        return view('contatos.form', compact('contato'));
    }

    public function update($id, ContatoRequest $request)
    {
        $contato = $this->contatoRepository->find($id);
        $this->authorize('update', $contato);

        $this->contatoRepository->update($contato, $request);

        return redirect()->route('contatos.show', $contato->id);
    }

    public function destroy($id)
    {
        $contato = $this->contatoRepository->find($id);
        $this->authorize('delete', $contato);

        $this->contatoRepository->destroy($contato);

        return redirect()->route('contatos.index');
    }
}
