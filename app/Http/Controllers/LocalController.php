<?php

namespace App\Http\Controllers;

use App\Http\Requests\LocalRequest;
use App\Models\Local;
use App\Repositories\LocalRepository;

class LocalController extends Controller
{
    function __construct(LocalRepository $localRepository)
    {
        $this->localRepository = $localRepository;
    }

    public function store(LocalRequest $request)
    {
        $local = $this->localRepository->store($request);

        return redirect()->route('pessoas.show', $local->pessoa->id);
    }

    public function destroy($id)
    {
        $local = $this->localRepository->find($id);
        $pessoa = $local->pessoa;

        $this->localRepository->destroy($local);

        return redirect()->route('pessoas.show', $pessoa->id);
    }
}
