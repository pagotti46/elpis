<?php
namespace App\Repositories;

use App\Http\Requests\ContatoRequest;
use App\Models\Contato;
use Auth;

class ContatoRepository
{
    public function find($id)
    {
        return Contato::findOrFail($id);
    }

    public function all()
    {
        return Auth::user()->contatos;
    }

    public function store(ContatoRequest $request)
    {
        $user = Auth::user();

        return $user->contatos()->create([
            'nome' => $request->input('nome'),
            'telefone' => $request->input('telefone'),
            'email' => $request->input('email'),
            'aceita_sms' => $request->input('aceita_sms') ? 1 : 0,
            'aceita_email' => $request->input('aceita_email') ? 1 : 0,
            'celular' => $request->input('celular'),
            'rg' => $request->input('rg'),
            'cpf' => $request->input('cpf'),
            'parentesco' => $request->input('parentesco'),
        ]);
    }

    public function update(Contato $contato, ContatoRequest $request)
    {
        $contato->fill([
            'nome' => $request->input('nome'),
            'telefone' => $request->input('telefone'),
            'email' => $request->input('email'),
            'aceita_sms' => $request->input('aceita_sms') ? 1 : 0,
            'aceita_email' => $request->input('aceita_email') ? 1 : 0,
            'celular' => $request->input('celular'),
            'rg' => $request->input('rg'),
            'cpf' => $request->input('cpf'),
            'parentesco' => $request->input('parentesco'),
        ]);

        $contato->save();
    }

    public function destroy(Contato $contato)
    {
        $contato->delete();
    }
}