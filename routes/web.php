<?php

use App\Http\Controllers\ContatoController;
use App\Http\Controllers\LocalController;
use App\Http\Controllers\PessoaController;
use App\Http\Controllers\UserController;
use App\Models\Pessoa;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/login');

Route::get('/dashboard', function () {
    $pessoas = Pessoa::where('isDesaparecido', 1)
                     ->orderBy('created_at', 'desc')
                     ->get();

    return view('dashboard', compact('pessoas'));
})->middleware(['auth'])->name('dashboard');

Route::get('/usuarios', [UserController::class, 'index'])->middleware(['auth'])->name('users.index');
Route::get('/usuarios/cadastrar', [UserController::class, 'create'])->middleware(['auth'])->name('users.create');
Route::post('/usuarios', [UserController::class, 'store'])->middleware(['auth'])->name('users.store');
Route::get('/usuarios/{id}', [UserController::class, 'show'])->middleware(['auth'])->name('users.show');
Route::get('/usuarios/{id}/editar', [UserController::class, 'edit'])->middleware(['auth'])->name('users.edit');
Route::post('/usuarios/{id}', [UserController::class, 'update'])->middleware(['auth'])->name('users.update');
Route::get('/usuarios/{id}/delete', [UserController::class, 'destroy'])->middleware(['auth'])->name('users.destroy');

Route::get('/pessoas', [PessoaController::class, 'index'])->middleware(['auth'])->name('pessoas.index');
Route::get('/pessoas/cadastrar', [PessoaController::class, 'create'])->middleware(['auth'])->name('pessoas.create');
Route::post('/pessoas/buscar', [PessoaController::class, 'buscar'])->middleware(['auth'])->name('pessoas.buscar');
Route::post('/pessoas', [PessoaController::class, 'store'])->middleware(['auth'])->name('pessoas.store');
Route::get('/pessoas/{id}', [PessoaController::class, 'show'])->middleware(['auth'])->name('pessoas.show');
Route::get('/pessoas/{id}/editar', [PessoaController::class, 'edit'])->middleware(['auth'])->name('pessoas.edit');
Route::post('/pessoas/{id}', [PessoaController::class, 'update'])->middleware(['auth'])->name('pessoas.update');
Route::get('/pessoas/{id}/delete', [PessoaController::class, 'destroy'])->middleware(['auth'])->name('pessoas.destroy');

Route::post('/locais', [LocalController::class, 'store'])->middleware(['auth'])->name('locais.store');
Route::get('/locais/{id}/delete', [LocalController::class, 'destroy'])->middleware(['auth'])->name('locais.destroy');

Route::get('/contatos', [ContatoController::class, 'index'])->middleware(['auth'])->name('contatos.index');
Route::get('/contatos/cadastrar', [ContatoController::class, 'create'])->middleware(['auth'])->name('contatos.create');
Route::post('/contatos', [ContatoController::class, 'store'])->middleware(['auth'])->name('contatos.store');
Route::get('/contatos/{id}', [ContatoController::class, 'show'])->middleware(['auth'])->name('contatos.show');
Route::get('/contatos/{id}/editar', [ContatoController::class, 'edit'])->middleware(['auth'])->name('contatos.edit');
Route::post('/contatos/{id}', [ContatoController::class, 'update'])->middleware(['auth'])->name('contatos.update');
Route::get('/contatos/{id}/delete', [ContatoController::class, 'destroy'])->middleware(['auth'])->name('contatos.destroy');

require __DIR__.'/auth.php';
