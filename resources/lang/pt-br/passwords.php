<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Sua senha foi redefinida!',
    'sent' => 'Nós te enviamos um email com um link para redefinição de sua senha!',
    'throttled' => 'Por favor espere antes de tentar novamente.',
    'token' => 'O token para redefinição de senha é inválido.',
    'user' => "Nós não encontramos um usuário com o e-mail fornecido.",

];
