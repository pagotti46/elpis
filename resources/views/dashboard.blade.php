<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Início') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="text-right">
                        <button class="inline-flex items-center px-4 py-2 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest disabled:opacity-25 transition ease-in-out duration-150 button-buscar mb-4 mr-1" data-bs-toggle="modal" data-bs-target="#busca">{{ __('web.buscar') }}</button>
                    </div>
                    <div class="row row-cols-1 row-cols-md-4 g-4">
                        @forelse($pessoas as $pessoa)
                        <div class="col">
                            <div class="card h-100">
                                <img src="/{{ $pessoa->foto }}" class="card-img-top" alt="foto-{{ Str::slug($pessoa->nome) }}">
                                <div class="card-body">
                                    <h5 class="card-title">{{$pessoa->nome}}, @idade($pessoa->dataNascimento)</h5>
                                    <p class="card-text">{{ Str::limit($pessoa->vestimentas, 250) }}</p>
                                    <a href="{{ route('pessoas.show', $pessoa->id) }}" class="inline-flex items-center px-4 py-2 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest disabled:opacity-25 transition ease-in-out duration-150 button-default float-right mt-2">{{ __('web.detalhes') }}</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="busca" tabindex="-1" aria-labelledby="buscaLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="buscaLabel">{{ __('web.buscar') }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="POST" action="{{ route('pessoas.buscar') }}" enctype="multipart/form-data">
                    <div class="modal-body">
                        @csrf
                        <div class="mb-3">
                            <div>
                                <x-label for="foto" :value="__('web.foto')" />

                                <x-input id="foto" class="block mt-1 w-full" type="file" name="foto" required/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="inline-flex items-center px-4 py-2 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest disabled:opacity-25 transition ease-in-out duration-150 btn-secondary" data-bs-dismiss="modal">{{ __('web.fechar') }}</button>
                        <button type="submit" class="inline-flex items-center px-4 py-2 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest disabled:opacity-25 transition ease-in-out duration-150 button-buscar">{{ __('web.buscar') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
