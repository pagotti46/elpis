<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('web.contatos') }}
        </h2>
    </x-slot>

    <x-card>
        <div class="float-right">
            @can('update', $contato)<a href="{{ route('contatos.edit', $contato->id) }}" class="btn btn-outline-primary">{{ __('web.editar') }}</a>@endcan
            @can('delete', $contato)<a href="{{ route('contatos.destroy', $contato->id) }}" class="btn btn-outline-danger" onclick="return confirm('Tem certeza que deseja deletar esse contato?')">{{ __('web.deletar') }}</a>@endcan
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="col-12">
                    <dt>{{ __('web.nome') }}</dt>
                    <dd>{{ $contato->nome }}</dd>
                </div>

                <div class="col-12 mt-2">
                    <dt>{{ __('web.parentesco') }}</dt>
                    <dd>{{ $contato->parentesco }}</dd>
                </div>

                <div class="col-12 mt-2">
                    <dt>{{ __('web.rg') }}</dt>
                    <dd>@rg($contato->rg)</dd>
                </div>

                <div class="col-12 mt-2">
                    <dt>{{ __('web.cpf') }}</dt>
                    <dd>@cpf($contato->cpf)</dd>
                </div>

                <div class="col-12 mt-2">
                    <dt>{{ __('web.email') }}</dt>
                    <dd>{{ $contato->email }}</dd>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-12 mt-2">
                    <dt>{{ __('web.celular') }}</dt>
                    <dd>@celular($contato->celular)</dd>
                </div>
                <div class="col-12 mt-2">
                    <dt>{{ __('web.telefone') }}</dt>
                    <dd>@telefone($contato->telefone)</dd>
                </div>
                <div class="col-12 mt-2">
                    <dt>{{ __('web.aceita_sms') }}</dt>
                    <dd>@if($contato->aceita_sms) Sim @else Não @endif</dd>
                </div>

                <div class="col-12 mt-2">
                    <dt>{{ __('web.aceita_email') }}</dt>
                    <dd>@if($contato->aceita_email) Sim @else Não @endif</dd>
                </div>
            </div>
        </div>
    </x-card>
</x-app-layout>
