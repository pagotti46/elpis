<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('web.pessoas') }}
        </h2>
    </x-slot>

    <x-card>
        <div class="float-right">
            @can('update', $pessoa)<a href="{{ route('pessoas.edit', $pessoa->id) }}" class="btn btn-outline-primary">{{ __('web.editar') }}</a>@endcan
            @can('delete', $pessoa)<a href="{{ route('pessoas.destroy', $pessoa->id) }}" class="btn btn-outline-danger" onclick="return confirm('Tem certeza que deseja deletar essa pessoa?')">{{ __('web.deletar') }}</a>@endcan
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="col-12">
                    <img src="/{{ $pessoa->foto }}" class="img-fluid w-25">
                    <dd>{{ $pessoa->nome }}</dd>
                </div>

                <div class="col-12 mt-2">
                    <dt>{{ __('web.dataNascimento') }} / {{ __('web.idade') }}</dt>
                    <dd>{{ $pessoa->dataNascimento->format('d/m/Y') }} - @idade($pessoa->dataNascimento)</dd>
                </div>

                <div class="col-12 mt-2">
                    <dt>{{ __('web.rg') }}</dt>
                    <dd>@rg($pessoa->rg)</dd>
                </div>

                <div class="col-12 mt-2">
                    <dt>{{ __('web.cpf') }}</dt>
                    <dd>@cpf($pessoa->cpf)</dd>
                </div>

                <div class="col-12 mt-2">
                    <dt>{{ __('web.corPele') }}</dt>
                    <dd>{{ $pessoa->corPele }}</dd>
                </div>

                <div class="col-12 mt-2">
                    <dt>{{ __('web.corOlho') }}</dt>
                    <dd>{{ $pessoa->corOlho }}</dd>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-12 mt-2">
                    <dt>{{ __('web.corCabelo') }}</dt>
                    <dd>{{ $pessoa->corCabelo }}</dd>
                </div>

                <div class="col-12 mt-2">
                    <dt>{{ __('web.altura') }}</dt>
                    <dd>{{ $pessoa->altura }} cm</dd>
                </div>

                <div class="col-12 mt-2">
                    <dt>{{ __('web.vestimentas') }}</dt>
                    <dd>{{ $pessoa->vestimentas }}</dd>
                </div>

                <div class="col-12 mt-2">
                    <dt>{{ __('web.sexo') }}</dt>
                    <dd>{{ __('web.sexo_'.$pessoa->sexo) }}</dd>
                </div>

                <div class="col-12 mt-2">
                    <dt>{{ __('web.desaparecido') }}</dt>
                    <dd>@if($pessoa->isDesaparecido) Sim @else Não @endif</dd>
                </div>

                <div class="col-12 mt-2">
                    <dt><a class="text-sm text-gray-700 underline" data-bs-toggle="modal" data-bs-target="#contatos" style="cursor: pointer;">+ {{ __('web.contatos') }}</a></dt>
                </div>
            </div>
        </div>
    </x-card>
    @can('update', $pessoa)
    @include('locais.form')
    <br>
    <br>
    @endcan
    @foreach($pessoa->locais->sortByDesc('created_at') as $local)
    @include('locais.show')
    @endforeach

    <div class="modal fade" id="contatos" tabindex="-1" aria-labelledby="contatosLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="contatosLabel">{{ __('web.contatos') }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    @forelse($pessoa->user->contatos as $key => $contato)
                    <div class="p-1">
                        <div class="row mb-2">
                            <h2><b>{{ $key+1 }}º Contato</b></h2>
                        </div>
                        <div class="row mb-4">
                            <div class="col-6">
                                <dt>{{ __('web.nome') }} <small class="text-muted">({{ __('web.parentesco') }})</small></dt>
                                <dd>{{ $contato->nome }} <small class="text-muted">({{ $contato->parentesco }})</small></dd>
                            </div>
                            <div class="col-6">
                                <dt>{{ __('web.email') }}</dt>
                                <dd>{{ $contato->email }}</dd>
                            </div>
                            <div class="col-6 mt-2">
                                <dt>{{ __('web.celular') }}</dt>
                                <dd>@celular($contato->celular)</dd>
                            </div>
                            <div class="col-6 mt-2">
                                <dt>{{ __('web.telefone') }}</dt>
                                <dd>@telefone($contato->telefone)</dd>
                            </div>
                        </div>
                    </div>
                    @empty
                    Nenhum contato cadastrado
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
