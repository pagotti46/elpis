<table class="table table-striped text-center">
    <thead>
        {{ $header }}
    </thead>
    <tbody>
        {{ $body }}
    </tbody>
</table>