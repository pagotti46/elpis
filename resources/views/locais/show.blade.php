<x-card class="py-2">
    <div class="col-md-1 position-relative float-right">
        <a href="{{ route('locais.destroy', $local->id) }}" class="btn btn-outline-danger position-absolute right-0" onclick="return confirm('Tem certeza que deseja deletar esse local?')">{{ __('web.deletar') }}</a>
    </div>
    <div class="row">
        <div class="col-md-4">
            <dt>{{ __('web.cep') }}</dt>
            <dd>@cep($local->cep)</dd>
        </div>
        <div class="col-md-4">
            <dt>{{ __('web.cidade') }}</dt>
            <dd>{{ $local->cidade }}</dd>
        </div>
        <div class="col-md-4">
            <dt>{{ __('web.estado') }}</dt>
            <dd>{{ $local->estado }}</dd>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-4">
            <dt>{{ __('web.logradouro') }}</dt>
            <dd>{{ $local->logradouro }}</dd>
        </div>

        <div class="col-md-1">
            <dt>{{ __('web.numero') }}</dt>
            <dd>{{ $local->numero }}</dd>
        </div>
        <div class="col-md-3">
            <dt>{{ __('web.bairro') }}</dt>
            <dd>{{ $local->bairro }}</dd>
        </div>
        <div class="col-md-4">
            <dt>{{ __('web.complemento') }}</dt>
            <dd>{{ $local->complemento ?? '-' }}</dd>
        </div>
    </div>
</x-card>