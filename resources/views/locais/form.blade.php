<x-card class="py-0">
        <x-auth-validation-errors class="mb-4" :errors="$errors" />
    <form method="POST" action="{{ route('locais.store') }}">
        @csrf
        <x-input id="pessoa" type="hidden" name="pessoa_id" :value="$pessoa->id"/>

        <div class="row">
            <div class="col-md-4">
                <x-label for="cep">
                    {{ __('web.cep') }} <small class="text-muted">Sem pontos e hífens</small>
                </x-label>

                <x-input id="cep" class="block mt-1 w-full" type="text" name="cep" required />
            </div>
            <div class="col-md-4">
                <x-label for="cidade">
                    {{ __('web.cidade') }}
                </x-label>

                <x-input id="cidade" class="block mt-1 w-full" type="text" name="cidade" required />
            </div>
            <div class="col-md-4">
                <x-label for="estado">
                    {{ __('web.estado') }}
                </x-label>

                <x-input id="estado" class="block mt-1 w-full" type="text" name="estado" required />
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-4">
                <x-label for="logradouro" :value="__('web.logradouro')" />

                <x-input id="logradouro" class="block mt-1 w-full" type="text" name="logradouro" required />
            </div>

            <div class="col-md-1">
                <x-label for="numero">
                    {{ __('web.numero') }}
                </x-label>

                <x-input id="numero" class="block mt-1 w-full" type="text" name="numero" required />
            </div>
            <div class="col-md-3">
                <x-label for="bairro">
                    {{ __('web.bairro') }}
                </x-label>

                <x-input id="bairro" class="block mt-1 w-full" type="text" name="bairro" required />
            </div>
            <div class="col-md-4">
                <x-label for="complemento">
                    {{ __('web.complemento') }}
                </x-label>

                <x-input id="complemento" class="block mt-1 w-full" type="text" name="complemento" />
            </div>
        </div>
        <div class="flex items-center justify-end mt-4">
            <x-button class="ml-4 btn btn-outline-success">
                {{ __('Salvar') }}
            </x-button>
        </div>
    </form>
</x-card>