<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('web.users') }}
        </h2>
    </x-slot>

    <x-card>
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="@isset($user){{ route('users.update', $user->id) }}@else{{ route('users.store') }}@endisset">
            @csrf
            @isset($user)
                <x-input type="hidden" name="id" :value="$user->id"/>
            @endisset

            <div>
                <x-label for="name" :value="__('web.nome')" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="$user->name ?? old('name')" required autofocus />
            </div>

            <div class="mt-4">
                <x-label for="email" :value="__('web.email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="$user->email ?? old('email')" required />
            </div>

            @if(Auth::user()->permissao == 'admin')
            <div class="mt-4">
                <x-label for="permissao" :value="__('web.permissao')" />

                <x-select id="permissao" class="block mt-1 w-full" name="permissao" required>
                    <option value='admin' @if(isset($user) && $user->permissao == 'admin') selected @endif>{{ __('web.admin') }}</option>
                    <option value='user' @if(isset($user) && $user->permissao == 'user') selected @endif>{{ __('web.user') }}</option>
                </x-select>
            </div>
            @endif

            <div class="mt-4">
                <x-label for="password" :value="__('web.senha')" />

                <x-input id="password" class="block mt-1 w-full" type="password" name="password" autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('web.senha_confirmation')" />

                <x-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" />
            </div>

            <div class="flex items-center justify-end mt-4">
                <x-button class="ml-4 btn btn-outline-success">
                    {{ __('Salvar') }}
                </x-button>
            </div>
        </form>
    </x-card>
</x-app-layout>
