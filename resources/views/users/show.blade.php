<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('web.users') }}
        </h2>
    </x-slot>

    <x-card>
        <div class="float-right">
            @can('update', $user)<a href="{{ route('users.edit', $user->id) }}" class="btn btn-outline-primary">{{ __('web.editar') }}</a>@endcan
            @can('delete', $user)<a href="{{ route('users.destroy', $user->id) }}" class="btn btn-outline-danger" onclick="return confirm('Tem certeza que deseja deletar esse usuário?')">{{ __('web.deletar') }}</a>@endcan
        </div>
        <div>
            <dt>{{ __('web.nome') }}</dt>
            <dd>{{ $user->name }}</dd>
        </div>
        <div class="pt-3">
            <dt>{{ __('web.email') }}</dt>
            <dd>{{ $user->email }}</dd>
        </div>
        <div class="pt-3">
            <dt>{{ __('web.permissao') }}</dt>
            <dd>{{ __('web.'.$user->permissao) }}</dd>
        </div>
    </x-card>
</x-app-layout>
